<?php

class subsAnetUser{
  public function __construct($account){
    //Accept either user object or user id
    if(is_numeric($account)) $account = user_load($account);
    $this->account = $account;
    $this->api = subs_anet_load_api();
  }

  //Return the Accounts' A.net profile id.
  public function getProfileId(){
    //If it has been saved to the user data array, return this.
    if(isset($this->account->data['subs_anet']['profileId'])){
      return $this->account->data['subs_anet']['profileId'];
    }
    //else check if a payment profile has it stored
    elseif($id = db_query('SELECT p.profile_id FROM {subs_anet_profiles} p WHERE uid = :uid', array(':uid' => $this->account->uid))->fetchField()){
      $this->account->data['subs_anet']['profileId'] = $id;
      user_save($this->account);
      return $id;
    }
    //else try to generate a profile
    else{
      $id = $this->createProfile();
      $this->account->data['subs_anet']['profileId'] = $id;
      user_save($this->account);
      return $id;
    }
  }

  //Return the Accounts' A.net last payment profile id.
  public function getLastPaymentProfileId(){
    $id = db_query('SELECT p.card_id FROM {subs_anet_profiles} p WHERE uid = :uid ORDER BY p.created DESC', array(':uid' => $this->account->uid))->fetchField();
    if($id){
      return $id;
    }
    else{
      dpm('no Payment Profile!');
      return false;
    }
  }

  //Return all of an Accounts' A.net payment profiles.
  public function getPaymentProfiles(){
    $paymentProfiles = db_query('SELECT p.* FROM {subs_anet_profiles} p WHERE uid = :uid ORDER BY p.created DESC', array(':uid' => $this->account->uid))->fetchAll();
    if($paymentProfiles){
      return $paymentProfiles;
    }
    else{
      dpm('no Payment Profile!');
      return false;
    }
  }

  //Create a a.net profile for a user.
  public function createProfile($message = ''){
    //Run the api request thruogh the subsAnetAPI object.
    $res = $this->api->createProfile($this->account->uid, $message);

    //If no response, return false
    if(!$res){
      dpm('no response');
      return false;
    }
    //Else if there was an error try to handle it
    elseif($res->messages->resultCode == 'Error'){
      dpm('Error');

      //If the error is E00039, then it is a duplicate request and we can extract the profile id from the error message.
      if($res->messages->message->code == 'E00039'){
        preg_match('|(\d)+|', $res->messages->message->text, $matches);
        $id = $matches[0];
      }

      //error was not handled, then return false.
      if(!isset($id)) return false;
      else return $id;
    }
    //A Response with no error implies success! Store the profile ID
    else{
      dpm('success');
      $id = $res->customerProfileId;
    }

    //If an ID was found, save it to the user data array.
    if($id){
      $this->account->data['subs_anet']['profileId'] = $id;
      user_save($this->account);
    }

    //Return the ID
    return $id;
  }

  public function updateProfile($message = ''){
    $profile = $this->api->updateProfile($this->account->uid, $this->getProfileId(), $message);
    return $profile;
  }

  public function deleteProfile(){
    $res = $this->api->deleteProfile($this->getProfileId());
    if($res->messages->resultCode != 'Error'){
      unset($this->account->data['subs_anet']['profileId']);
      user_save($this->account);
    }
    return $res;
  }

  public function createPaymentProfile($billingAddress, $creditCard){
    $profileId = $this->getProfileId();
    $res = $this->api->createPaymentProfile($profileId, $billingAddress, $creditCard);
    if(!$res){
      dpm('no response');
      return false;
    }
    elseif($res->messages->resultCode == 'Error'){
      dpm('Error');

      //"The record cannot be found" => need to create profile for user
      if($res->messages->message->code == 'E00040'){
        if(!isset($this->recursionBlocker->E00040)){
          $this->recursionBlocker->E00040 = 1;
          $pid = $this->createProfile();
          $id = $this->createPaymentProfile($billingAddress, $creditCard);
        }
        else{
          dpm('double E00040 error.  Recursion blocked.');
        }
      }
      if(!isset($id)) return false;
    }
    else{
      dpm('success');
      $id = $res->customerPaymentProfileId;
    }

    if($id){
      $exp = strtotime($creditCard['expirationDate']);
      $card_number = $creditCard['cardNumber'];
      $card_name = $billingAddress['firstName'] ? $billingAddress['firstName'] : '';
      $card_name .= $billingAddress['lastName'] ? ' '.$billingAddress['lastName'] : '';
      $card_type = $this->getCardType($creditCard['cardNumber']);
      $card_exp_month = date('m', $exp);
      $card_exp_year = date('Y', $exp);
      $status = 1;
      $created = time();
      $changed = $created;

      $s = db_merge('subs_anet_profiles')
        ->key(array('card_id' => $id))
        ->fields(array(
            'uid' => $this->account->uid,
            'profile_id' => $this->getProfileId(),
            'card_type' => $card_type,
            'card_name' => $card_name,
            'card_number' => substr($card_number, -4),
            'card_exp_month' => $card_exp_month,
            'card_exp_year' => $card_exp_year,
            'status' => $status,
            'created' => $created,
            'changed' => $changed,
          )
        )
        ->execute();
    }

    return $id;
  }

  public function updatePaymentProfile($paymentProfileId, $billingAddress, $creditCard){
    $res = $this->api->updatePaymentProfile($this->getProfileId(), $paymentProfileId, $billingAddress, $creditCard);
    
    if(!$res){
      dpm('no response');
      return false;
    }
    elseif($res->messages->resultCode == 'Error'){
      dpm('Error');

      //"The record cannot be found" => need to create profile for user
      if($res->messages->message->code == 'E00040'){
        if(!isset($this->recursionBlocker->E00040)){
          $this->recursionBlocker->E00040 = 1;
          $profileId = $this->createProfile();
          $res2 = $this->createPaymentProfile($billingAddress, $creditCard);
        }
        else{
          dpm('double E00040 error.  Recursion blocked.');
        }
      }
      if(!isset($id)) return false;
    }
    else{
      if($paymentProfileId){
        $exp = strtotime($creditCard['expirationDate']);
        $card_number = $creditCard['cardNumber'];
        $card_name = $billingAddress['firstName'] ? $billingAddress['firstName'] : '';
        $card_name .= $billingAddress['lastName'] ? ' '.$billingAddress['lastName'] : '';
        $card_type = $this->getCardType($creditCard['cardNumber']);
        $card_exp_month = date('m', $exp);
        $card_exp_year = date('Y', $exp);
        $status = 1;
        $created = time();
        $changed = $created;

        $s = db_merge('subs_anet_profiles')
          ->key(array('card_id' => $paymentProfileId))
          ->fields(array(
              'uid' => $this->account->uid,
              'profile_id' => $this->getProfileId(),
              'card_type' => $card_type,
              'card_name' => $card_name,
              'card_number' => substr($card_number, -4),
              'card_exp_month' => $card_exp_month,
              'card_exp_year' => $card_exp_year,
              'status' => $status,
              'created' => $created,
              'changed' => $changed,
            )
          )
          ->execute();
      }
    }

    return $paymentProfileId;
  }

  public function deletePaymentProfile($paymentProfileId){
    $res = $this->api->deletePaymentProfile($this->getProfileId(), $paymentProfileId);
    if($res->messages->resultCode != 'Error'){
      $s = db_delete('subs_anet_profiles')
        ->condition('card_id', $paymentProfileId)
        ->execute();
    }
    return $res;
  }


  //Charge a customer using a provided payment profile.
  public function charge($amount, $paymentProfile = null, $order = array(), $lineItems = array()){
    $paymentProfileId = isset($paymentProfile) ? $paymentProfile : $this->getLastPaymentProfileId();
    $res = $this->api->charge($this->getProfileId(), $paymentProfile, $amount, $order, $lineItems);
    return $res;
  }



  public function getCardType($ccNum){
    /*
        * mastercard: Must have a prefix of 51 to 55, and must be 16 digits in length.
        * Visa: Must have a prefix of 4, and must be either 13 or 16 digits in length.
        * American Express: Must have a prefix of 34 or 37, and must be 15 digits in length.
        * Diners Club: Must have a prefix of 300 to 305, 36, or 38, and must be 14 digits in length.
        * Discover: Must have a prefix of 6011, and must be 16 digits in length.
        * JCB: Must have a prefix of 3, 1800, or 2131, and must be either 15 or 16 digits in length.
    */

    if (preg_match("/^5[1-5][0-9]{14}$/", $ccNum))
            return "Mastercard";

    if (preg_match("/^4[0-9]{12}([0-9]{3})?$/", $ccNum))
            return "Visa";

    if (preg_match("/^3[47][0-9]{13}$/", $ccNum))
            return "American Express";

    if (preg_match("/^3(0[0-5]|[68][0-9])[0-9]{11}$/", $ccNum))
            return "Diners Club";

    if (preg_match("/^6011[0-9]{12}$/", $ccNum))
            return "Discover";

    if (preg_match("/^(3[0-9]{4}|2131|1800)[0-9]{11}$/", $ccNum))
            return "JCB";
    else return 'und';
  }
}