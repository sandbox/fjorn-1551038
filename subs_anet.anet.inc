<?php

define('SUBS_ANET_XMNLS', 'AnetApi/xml/v1/schema/AnetApiSchema.xsd');
define('SUBS_ANET_TXN_MODE_LIVE', 'live');
define('SUBS_ANET_TXN_MODE_LIVE_TEST' , 'live_test');
define('SUBS_ANET_TXN_MODE_DEVELOPER' , 'developer');
define('SUBS_ANET_CIM_APPROVED', 1);
define('SUBS_ANET_CIM_DECLINED', 2);
define('SUBS_ANET_CIM_ERROR', 3);
define('SUBS_ANET_CIM_HELD', 4);

/**
 * anetAPI class.  
 * Each anetAPI object is a gateway for interacting with the authorize.net server.
 * All application logic belongs in the source code that is calling the API.
 */

class subsAnetAPI {

  //Constructor.  Sets key variables that will not change over the lifetime of this object.
  function __construct(){
    //txnX variables refer to values that are needed in generating the transaction xml.
    $this->txnMode = variable_get('subs_anet_txn_mode');
    $this->txnKey = variable_get('subs_anet_txn_key');
    $this->txnName = variable_get('subs_anet_txn_name');
    $this->txnUrl = $this->cimUrl($this->txnMode);
    $this->txnValidationMode = $this->cimValidationMode($this->txnMode);
  }

  //Create Profile
  //Call createCustomerProfileRequest A.Net request
  //Generates a CIM Profile.  Returns structured object for handling by caller.
  public function createProfile($uid, $desc = ''){
    $account = user_load($uid);
    $merchantAuth = $this->buildMerchantAuth();
    $data= array(
      'profile' => array(
        'merchantCustomerId' => $uid,
        'description' => $desc,
        'email' => $account->mail,
      ),
    );

    //createCustomerProfileRequest requires validationMode be none unless there is a payment profile included (which we handle seperately).
    $validationMode = array('validationMode' => 'none');
    
    //Build the simpleXML request object
    $req = $this->buildXML('createCustomerProfileRequest', $merchantAuth, $data, $validationMode);
    
    //Send it off
    $res = $this->cimRequest($req);
    return $this->simplexmlToObj($res);
  }

  public function updateProfile($uid, $profileId, $desc = ''){
    $account = user_load($uid);
    $merchantAuth = $this->buildMerchantAuth();
    $data = array(
      'profile' => array(
        'merchantCustomerId' => $account->uid,
        'description' => $desc,
        'email' => $account->mail,
        'customerProfileId' => $profileId,
      ),
    );

    //Build the simpleXML request object
    $req = $this->buildXML('updateCustomerProfileRequest', $merchantAuth, $data);
    
    //Send it off
    $res = $this->cimRequest($req);
    return $this->simplexmlToObj($res);
  }

  public function deleteProfile($profileId){
    $merchantAuth = $this->buildMerchantAuth();
    $data = array(
      'customerProfileId' => $profileId,
    );

    //Build the simpleXML request object
    $req = $this->buildXML('deleteCustomerProfileRequest', $merchantAuth, $data);
    
    //Send it off
    $res = $this->cimRequest($req);
    return $this->simplexmlToObj($res);
  }

  //Create Payment Profile
  //Call createCustomerPaymentProfileRequest A.Net request
  //Generates a CIM Profile.  Returns structured object for handling by caller.
  public function createPaymentProfile($profileId, $billingAddress, $creditCard){
    if(!isset($creditCard['cardNumber']) || !isset($creditCard['expirationDate'])){
      dpm('Credit Card Number or Expiration Date missing from request');
      return false;
    }
    $merchantAuth = $this->buildMerchantAuth();
    $data = array(
      'customerProfileId' => $profileId,
      'paymentProfile' => array(
        'billTo' => $billingAddress,
        'payment' => array(
          'creditCard' => $creditCard
        ),
      ),
    );

    //createCustomerProfileRequest requires validationMode be none unless there is a payment profile included (which we handle seperately).
    $validationMode = array('validationMode' => $this->cimValidationMode());
    
    //Build the simpleXML request object
    $req = $this->buildXML('createCustomerPaymentProfileRequest', $merchantAuth, $data, $validationMode);

    //Send it off
    $res = $this->cimRequest($req);
    return $this->simplexmlToObj($res);
  }

  public function updatePaymentProfile($profileId, $paymentProfileId, $billingAddress, $creditCard){
    $merchantAuth = $this->buildMerchantAuth();
    $data = array(
      'customerProfileId' => $profileId,
      'paymentProfile' => array(
        'billTo' => $billingAddress,
        'payment' => array(
          'creditCard' => $creditCard
        ),
        'customerPaymentProfileId' => $paymentProfileId,
      ),
    );

    //createCustomerProfileRequest requires validationMode be none unless there is a payment profile included (which we handle seperately).
    $validationMode = array('validationMode' => $this->cimValidationMode());

    //Build the simpleXML request object
    $req = $this->buildXML('updateCustomerPaymentProfileRequest', $merchantAuth, $data, $validationMode);
    
    //Send it off
    $res = $this->cimRequest($req);
    return $this->simplexmlToObj($res);
  }

  public function deletePaymentProfile($profileId, $paymentProfileId){
    $merchantAuth = $this->buildMerchantAuth();
    $data = array(
      'customerProfileId' => $profileId,
      'customerPaymentProfileId' => $paymentProfileId,
    );

    //createCustomerProfileRequest requires validationMode be none unless there is a payment profile included (which we handle seperately).

    //Build the simpleXML request object
    $req = $this->buildXML('deleteCustomerPaymentProfileRequest', $merchantAuth, $data);
    
    //Send it off
    $res = $this->cimRequest($req);
    return $this->simplexmlToObj($res);
  }

  //Authorize and Charge a Payment Profile
  //Call createCustomerProfileTransactionReques A.Net request
  //Generates a CIM Profile.  Returns structured object for handling by caller.
  public function charge($profileId, $paymentProfileId, $amount, $order = array(), $lineItems = array()){
    $merchantAuth = $this->buildMerchantAuth();
    $data = array(
      'transaction' => array(
        'profileTransAuthCapture' => array(
          'amount' => $amount,
          'customerProfileId' => $profileId,
          'customerPaymentProfileId' => $paymentProfileId,
        ),
      ),
    );
    if($order) $data['transaction']['profileTransAuthCapture']['order'] = $order;

    //Build the simpleXML request object
    $req = $this->buildXML('createCustomerProfileTransactionRequest', $merchantAuth, $data);
    
    //Line items not working right now.  Problem with a.net API?
    //$this->simplexmlAddChildren($req->transaction->profileTransAuthCapture, $lineItems, 'lineItems');

    //Send it off
    $res = $this->cimRequest($req);
    return $res;
  }

  //Build the merchantAuthentication xml element (common to most API calls)
  private function buildMerchantAuth(){
    $merchantAuthentication = array(
      'merchantAuthentication' => array(
        'name' => $this->txnName,
        'transactionKey' => $this->txnKey,
      ),
    );
    return $merchantAuthentication;
  }

  //Build the XML object.  First argument is the root element, and all other arguments are arrays or objects to be folded in.
  private function buildXML($base){
    $args = func_get_args();
    array_shift($args);

    $req = new SimpleXMLElement('<'.$base.' />');
    $req->addAttribute('xmlns', SUBS_ANET_XMNLS);
    foreach($args as $block){
      $this->simplexmlAddChildren($req, $block);
    }

    return $req;
  }


  //Send the XML request via cURL
  private function cimRequest($req){
    $xml = $req->asXML();
    dpm($xml);
    $header = array();
    $header[] = 'Content-type: text/xml; charset=utf-8';
    $header[] = 'Content-length: ' . strlen($xml);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->txnUrl);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_NOPROGRESS, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $result = curl_exec($ch);

    if ($error = curl_error($ch)) {
      dpm($error);
      dpm('ERRROR');
      watchdog('commerce_authnet', 'cURL error: @error', array('@error' => $error), WATCHDOG_ERROR);
      return FALSE;
    }
    curl_close($ch);
    if (!empty($result)) {
      // Remove non-absolute XML namespaces to prevent SimpleXML warnings.
      $result = str_replace(' xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd"', '', $result);

      // Extract the result into an XML response object.
      $response = $this->processResult($result);

      return $response;
    }
    else {
      dpm('no response');
      return FALSE;
    }
  }

  //Returns the API URL that we will be communicating with (url depend on transaction mode)
  private function cimUrl($txn_mode = null) {
    if(!$txn_mode) $txn_mode = $this->txnMode;
    switch ($txn_mode) {
      case SUBS_ANET_TXN_MODE_LIVE:
      case SUBS_ANET_TXN_MODE_LIVE_TEST:
        return 'https://api.authorize.net/xml/v1/request.api';
      case SUBS_ANET_TXN_MODE_DEVELOPER:
        return 'https://apitest.authorize.net/xml/v1/request.api';
    }
  }

  //Returns the default validation mode. Some methods require their own validation handling, but this establishes the default. 
  private function cimValidationMode($txn_mode = null) {
    if(!$txn_mode) $txn_mode = $this->txnMode;
    switch ($txn_mode) {
      case SUBS_ANET_TXN_MODE_LIVE:
        return 'liveMode';
      case SUBS_ANET_TXN_MODE_LIVE_TEST:
      case SUBS_ANET_TXN_MODE_DEVELOPER:
        return 'testMode';
    }
  }

  private function processResult($result){
    $res = new SimpleXMLElement($result);
    $resObj = $this->simplexmlToObj($res);
    $direct = false;
    if(isset($resObj->directResponse)) $direct = $resObj->directResponse;
    elseif(isset($resObj->validationDirectResponse)) $direct = $resObj->validationDirectResponse;
    if($direct){
      $pieces = explode(',', $direct);
      $resObj->data->response_code        = $pieces[0];
      $resObj->data->response_subcode     = $pieces[1];
      $resObj->data->response_reason_code = $pieces[2];
      $resObj->data->response_reason_text = $pieces[3];
      $resObj->data->authorization_code   = $pieces[4];
      $resObj->data->avs_response         = $pieces[5];
      $resObj->data->transaction_id       = $pieces[6];
      $resObj->data->invoice_number       = $pieces[7];
      $resObj->data->description          = $pieces[8];
      $resObj->data->amount               = $pieces[9];
      $resObj->data->method               = $pieces[10];
      $resObj->data->transaction_type     = $pieces[11];
      $resObj->data->customer_id          = $pieces[12];
      $resObj->data->first_name           = $pieces[13];
      $resObj->data->last_name            = $pieces[14];
      $resObj->data->company              = $pieces[15];
      $resObj->data->address              = $pieces[16];
      $resObj->data->city                 = $pieces[17];
      $resObj->data->state                = $pieces[18];
      $resObj->data->zip_code             = $pieces[19];
      $resObj->data->country              = $pieces[20];
      $resObj->data->phone                = $pieces[21];
      $resObj->data->fax                  = $pieces[22];
      $resObj->data->email_address        = $pieces[23];
      $resObj->data->ship_to_first_name   = $pieces[24];
      $resObj->data->ship_to_last_name    = $pieces[25];
      $resObj->data->ship_to_company      = $pieces[26];
      $resObj->data->ship_to_address      = $pieces[27];
      $resObj->data->ship_to_city         = $pieces[28];
      $resObj->data->ship_to_state        = $pieces[29];
      $resObj->data->ship_to_zip_code     = $pieces[30];
      $resObj->data->ship_to_country      = $pieces[31];
      $resObj->data->tax                  = $pieces[32];
      $resObj->data->duty                 = $pieces[33];
      $resObj->data->freight              = $pieces[34];
      $resObj->data->tax_exempt           = $pieces[35];
      $resObj->data->purchase_order_number= $pieces[36];
      $resObj->data->md5_hash             = $pieces[37];
      $resObj->data->card_code_response   = $pieces[38];
      $resObj->data->cavv_response        = $pieces[39];
      $resObj->data->account_number       = $pieces[50];
      $resObj->data->card_type            = $pieces[51];
      $resObj->data->split_tender_id      = $pieces[52];
      $resObj->data->requested_amount     = $pieces[53];
      $resObj->data->balance_on_card      = $pieces[54];
      $resObj->data->approved = ($resObj->data->response_code == SUBS_ANET_CIM_APPROVED);
      $resObj->data->declined = ($resObj->data->response_code == SUBS_ANET_CIM_DECLINED);
      $resObj->data->error    = ($resObj->data->response_code == SUBS_ANET_CIM_ERROR);
      $resObj->data->held     = ($resObj->data->response_code == SUBS_ANET_CIM_HELD);
    }
    dpm($resObj);
    return $resObj;
  }

  /**
   * Adds child elements to a SimpleXML element using the data provided.
   *
   * @param $simplexml_element
   *   The SimpleXML element that will be populated with children from the child
   *   data array. This should already be initialized with its container element.
   * @param $child_data
   *   The array of data. It can be of any depth, but it only provides support for
   *   child elements and their values - not element attributes. If an element can
   *   have multiple child elements with the same name, you cannot depend on a
   *   simple associative array because of key collision. You must instead include
   *   each child element as a value array in a numerically indexed array.
   * @param $default_value
   *   The default value will be used as the node name if present and the node name would otherwise be numeric
   */
  private function simplexmlAddChildren(&$simplexml_element, $child_data, $default_value = null) {
    // Loop over all the child data...
    foreach ($child_data as $key => $value) {
      // If the current child is itself a container...
      if (is_array($value)) {
        // If it has a non-numeric key...
        if (!is_numeric($key)) {
          // Add a child element to the current element with the key as the name.
          $child_element = $simplexml_element->addChild("$key");

          // Add the value of this element to the child element.
          $this->simplexmlAddChildren($child_element, $value, $key);
        }
        else {
          // Otherwise assume we have multiple child elements of the same name and
          // pass through to add the child data from the current value array to
          // current element.
          $child_element = $simplexml_element->addChild($default_value);
          $this->simplexmlAddChildren($child_element, $value, $default_value);
        }
      }
      else {
        if(is_numeric($key) && $default_value){
          $simplexml_element->addChild("$default_value", "$value");
        }
        else{
        // Otherwise add the child element with its simple value.
        $simplexml_element->addChild("$key", "$value");
        }
      }
    }
  }

  //Convert simpleXML object to an Array.
  private function simplexmlToArray($xml, $root = true) {
    if (!$xml->children()) {
      return (string)$xml;
    }
    $array = array();
    foreach ($xml->children() as $element => $node) {
      $totalElement = count($xml->{$element});
      if (!isset($array[$element])) {
        $array[$element] = "";
      }
      if ($attributes = $node->attributes()) {
        $data = array(
          'attributes' => array(),
          'value' => (count($node) > 0) ? $this->simplexmlToArray($node, false) : (string)$node
          );
        foreach ($attributes as $attr => $value) {
          $data['attributes'][$attr] = (string)$value;
        }
        if ($totalElement > 1) {
          $array[$element][] = $data;
        } else {
          $array[$element] = $data;
        }
      } else {
        if ($totalElement > 1) {
          $array[$element][] = $this->simplexmlToArray($node, false);
        } else {
          $array[$element] = $this->simplexmlToArray($node, false);
        }
      }
    }
    if ($root) {
      return array($xml->getName() => $array);
    } else {
      return $array;
    }
  }

  //Convert simpleXML object to an Object.
  private function simplexmlToObj($xml, $root = true) {
    $data = json_decode(json_encode($xml));
    return $data;
  }

}